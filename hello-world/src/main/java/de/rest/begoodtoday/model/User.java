package de.rest.begoodtoday.model;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Data
public class User {

    public User (){

    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int user_id;

    @Size(min = 1,max = 250)
    private String user_name;

    @Size(min = 1,max = 250)
    private String user_password;


}

