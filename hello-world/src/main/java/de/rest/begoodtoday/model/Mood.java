package de.rest.begoodtoday.model;

import de.rest.begoodtoday.MoodGenerator;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Instant;

@Entity
@Data
public class Mood {

	public Mood(){
		mood = MoodGenerator.getRandomMood();
		timestamp = Instant.now();
		user_id = 1;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int mood_id;

	@Size(max=250)
	private String mood;

	private Instant timestamp;

	@JoinColumn(name = "user_id")
	@NotNull
	private int user_id;


}
