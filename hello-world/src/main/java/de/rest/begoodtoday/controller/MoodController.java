package de.rest.begoodtoday.controller;

import de.rest.begoodtoday.model.Mood;
import de.rest.begoodtoday.repository.MoodRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class MoodController {

    @Autowired
    MoodRepository moodRepository;

    @GetMapping(value="/mood/new")
    public Mood createNewMood ()
    {
        Mood newMood = new Mood();
        moodRepository.saveAndFlush(newMood);
        for (Mood m : moodRepository.findAll()){
            System.out.println(m.toString());
        }
        return newMood;

    }


}
