package de.rest.begoodtoday.controller;


import de.rest.begoodtoday.model.Mood;
//import de.rest.begoodtoday.model.User;
//import de.rest.begoodtoday.repository.MoodRepository;
//import de.rest.begoodtoday.repository.UserRepository;
import de.rest.begoodtoday.model.User;
import de.rest.begoodtoday.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    @Autowired
    UserRepository userRepository;

    @PostMapping(value = "/user/new")
    public User createUser(@RequestBody User user){
        userRepository.saveAndFlush(user);
        return user;
    }

}
